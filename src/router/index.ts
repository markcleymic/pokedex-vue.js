import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/pokemon',
    name: 'Pokemon',
    component: () => import('../views/Pokemon.vue')
  },
  {
    path: '/type',
    name: 'Type',
    component: () => import('../views/Type.vue')
  }
]

const router = new VueRouter({
  routes
})

export default router
